<link href="<?=base_url()?>/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url('/assets/global/highcharts.js')?>"></script>
<script src="<?=base_url('/assets/global/exporting.js')?>"></script>
<script src="<?=base_url()?>/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?=base_url()?>/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>

<!-- HEAD 1 -->
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" >
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="89"></span>asees</div>
                <div class="desc"> Pendapatan Per <?=tgl_format(date('Y-m-d'))?> </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" >
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="1349"></span>
                </div>
                <div class="desc"> Total Wajib Pajak </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" >
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="12,5"></span></div>
                <div class="desc"> Total Wajib Pajak Pending</div>
            </div>
        </a>
    </div>
</div>
<div class="clearfix"></div>

<!-- HEAD 2 -->
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 note note-info ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="1349">Rp. 10.000.000</span>
                    </h3>
                    <small>Target Pendapatan</small>
                </div>
                <!-- <div class="icon">
                    <i class="icon-like"></i>
                </div> -->
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 85%;" class="progress-bar progress-bar-success purple-soft">
                        <span class="sr-only">85% change</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> Presentase Target Pendapatan </div>
                    <div class="status-number"> 85% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 note note-info ">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="1349">60</span>
                    </h3>
                    <small>WP Yang Sudah Melaporkan SPTPD</small>
                </div>
                <!-- <div class="icon">
                    <i class="icon-like"></i>
                </div> -->
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 60%;" class="progress-bar progress-bar-success blue-sharp">
                        <span class="sr-only">60% change</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> Presentase SPTPD Terlapor </div>
                    <div class="status-number"> 60% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 note note-info ">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="1349">10</span>
                    </h3>
                    <small>SPTPD Kena Sanksi</small>
                </div>
                <!-- <div class="icon">
                    <i class="icon-like"></i>
                </div> -->
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 10%;" class="progress-bar progress-bar-success red-haze">
                        <span class="sr-only">10% change</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> Presentase SPTPD Kena Sanksi </div>
                    <div class="status-number"> 10% </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- HEAD 3 -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light note note-info">
            <div id="sptpd_null" style="min-width: 200px; height: 600px; margin: 0 auto"></div>
        </div>
    </div>
</div>

<!-- HEAD 4 -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light note note-info">
            <div id="sspd_total" style="min-width: 200px; height: 1500px; margin: 0 auto"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
   
    .done(function() {
        console.log("success");
    })
    .fail(function() {
        console.log("error");
    });

    Highcharts.chart('sptpd_null', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Jumlah SPTPD Yang Belum Disetorkan'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Walet', 'Restoran', 'Reklame', 'Penerangan Jalan', 'Parkir', 'Mineral Bukan Logam', 'Hotel', 'Hiburan', 'Air Tanah'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' SPTPD'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Tahun 2018',
            data: [107, 31, 635, 203, 10, 100, 200, 300, 37]
        }, {
            name: 'Tahun 2017',
            data: [133, 156, 947, 408, 6, 30, 40, 20, 10]
        }]
    });

    Highcharts.chart('sspd_total', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Pendapatan SSPD Perbulan'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Desember', 'November' ,'Oktober', 'September', 'Agustus', 'Juli', 'Juni', 'Mei', 'April', 'Maret', 'Februari', 'Januari'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Pendapatan',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' SSPD'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [
            {
                name: 'Air Tanah',
                data: [107, 31, 635, 203, 10, 100, 200, 300, 37, 23, 100, 123]
            },
            {
                name: 'Hotel',
                data: [133, 156, 550, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
            {
                name: 'Mineral Bukan Logam',
                data: [133, 156, 450, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
            {
                name: 'Parkir',
                data: [133, 156, 350, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
            {
                name: 'Penerangan Jalan',
                data: [133, 156, 600, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
            {
                name: 'Reklame',
                data: [133, 156, 500, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
            {
                name: 'Restoran',
                data: [133, 156, 400, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
            {
                name: 'Walet',
                data: [133, 156, 300, 408, 6, 30, 40, 20, 10, 20, 30, 10]
            },
        ]
    });

});
</script>
