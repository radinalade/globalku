<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_utama extends MX_Controller {

	public function index()
	{

		$data['pagetitle']  = 'Dashboard';
        $data['subtitle']   = 'Statistics Analysis';
        $js['js']           = null;
        $css['css']         = null;

        $this->template->display('master_utama', $data, $js, $css );
	}

}

/* End of file Menu_utama.php */
/* Location: ./application/modules/master/controllers/Menu_utama.php */