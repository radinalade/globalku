<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

    private $table_db       = 'user';
    private $table_prefix   = 'user_';

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
    }

    public function index()
    {
        /*if(get_cookie('fp23k')) {
            $cookie = get_cookie('fp23k');
            $result   = $this->m_global->get( $this->table_db, null, ['user_password' => $cookie]);
            $this->session->set_userdata('user_data', $result[0]);
            redirect( base_url().'landing' );
        }*/
        $data['pagetitle']  = 'Login Page';

        // $image    = @$this->m_global->get('master_image',NULL, ['master_image_aktif' => '1', 'master_image_status' => '1']);
        $image       = ['img1.jpeg','img2.jpeg','img3.jpeg'];

        $data['images'] = json_encode(array("image" => $image ));

        $this->load->view('login/login', $data);
    }

    public function cek_login()
    {
        $this->form_validation->set_rules('username',   'Username',     'trim|required');
        $this->form_validation->set_rules('password',   'Password',     'trim|required');

        if ( $this->form_validation->run( $this ) ){
            $user     = $this->input->post('username') == ' ' || $this->input->post('username') == '' ? 'user name' : $this->input->post('username');
            $password = $this->input->post('password');

            $cek_user = $this->db->query("SELECT * from user where user_name = '$user' or user_email = '$user' or no_daftar = '$user'")->row();

            if ($cek_user) {
				if($cek_user->user_status == "0"){
					$this->session->set_flashdata('status',
                                        '<div class="alert alert-danger">
                                            <button class="close" data-close="alert"></button>
                                            <span>User anda belum diaktivasi, mohon cek email anda</span>
                                        </div>');
                    redirect (base_url().'login' );
                }else if ($this->bcrypt->check_password($password, $cek_user->user_password)) {

                    if($this->input->post('remember') == 1){
                        $day = 60 * 60 * 24 * 7;
                        set_cookie("fp23k", $password, $day);
                    }

                    $this->session->set_userdata('user_data', $cek_user);
                    // echo "<pre>",print_r($this->session->all_userdata()),exit();

                    // data log
                    $log['detail']  = 'Login '.$this->session->user_data->user_name;
                    $log['action']  = 'Login';
                    $log['status']  = $this->session->user_data->user_status;
                    $log['user_id'] = $this->session->user_data->user_id;
                    
                    $log['ip']      = $_SERVER['REMOTE_ADDR'];

                    $this->db->insert('user_log', $log);
                    redirect( base_url().'master/menu_utama' );
                } else {
                    $this->session->set_flashdata('status',
                                        '<div class="alert alert-danger">
                                            <button class="close" data-close="alert"></button>
                                            <span>Password Anda Salah</span>
                                        </div>');
                    redirect (base_url().'login' );
                }
            } else {
                $this->session->set_flashdata('status',
                                    '<div class="alert alert-danger">
                                        <button class="close" data-close="alert"></button>
                                        <span>Username Tidak Ada</span>
                                    </div>');
                redirect (base_url().'login' );
            }
        }
    }

    function out()
    {
        if(get_cookie('fp23k')) {
            delete_cookie('fp23k');
        }
        $this->session->sess_destroy();
        redirect( base_url().'login');
    }
}
