<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MX_Controller
{
    private $prefix         = 'login/register';
    private $url            = 'login/register';
    private $base_url       = '';
    private $table_db       = 'user';
    private $path           = 'login/pajak/';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct() {
        parent::__construct();
		$this->load->model('M_register', 'mrg');
        $this->load->helper('cookie');
    }

    public function index()
    {
        if(get_cookie('fp23k')) {
            $cookie = get_cookie('fp23k');
            $result   = $this->m_global->get( $this->table_db, null, ['user_password' => $cookie]);
            $this->session->set_userdata('user_data', $result[0]);
            redirect( base_url().'landing' );
        }

        $data['pagetitle']  = 'Login Page';

        // $image    = @$this->m_global->get('master_image',NULL, ['master_image_aktif' => '1', 'master_image_status' => '1']);
        $image       = ['img1.jpeg','img2.jpeg','img3.jpeg'];

        $data['images'] = json_encode(array("image" => $image ));

        $this->load->view('login/register', $data);
    }

    public function action()
    {
        // echo "<pre>",print_r($this->input->post()),exit();
        
        $this->form_validation->set_rules('user_full_name', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('user_name', 'Nama Panggilan', 'trim|required|unique[user.user_name]');
        $this->form_validation->set_rules('user_email', 'Email', 'trim');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');
        $this->form_validation->set_rules('user_password2', 'Confirm Password', 'trim|required|matches[user_password]');

		if ( $this->form_validation->run( $this ) ){
            $this->db->trans_begin();
            $data['user_full_name']         = $this->input->post('user_full_name');
            $data['user_name']              = $this->input->post('user_name');
            $data['user_email']             = $this->input->post('user_email');
            $data['user_password']          = $this->bcrypt->hash_password($this->input->post('user_password'));
            $data['user_password']          = $this->bcrypt->hash_password($this->input->post('user_password'));
            $data['user_status']            = '0';
            $data['user_verification_code'] = sprintf("%06d", mt_rand(1, 999999));
          

            $result  = $this->m_global->insert( $this->table_db, $data );

            $log['detail']  = 'Daftar '.$this->input->post('user_full_name');
                    $log['action']  = 'Daftar';
                    $log['status']  = '0';
                    $log['user_id'] = $this->db->insert_id();
                    $log['ip']      = $_SERVER['REMOTE_ADDR'];
                    $this->db->insert('user_log', $log);
                   

            

            // echo $no_daftar;exit();

            $email  = $this->m_global->email($data['user_email'], 'Verifikasi Akun', 'Verifikasi Akun', 'Untuk memverifikasi user anda mohon inputkan kode dibawah ini<br>Kode Verifikasi : <a href="'.site_url('login/register/verifikasi_email/'.$data['user_name'].'/'.$data['user_verification_code']).'" target="_blank">'.$data['user_verification_code'].'</a>');
            echo $this->db->trans_status();
           
            if ($this->db->trans_status() === FALSE || !$email)
            {
                die('asda');
                $this->db->trans_rollback();
                $this->session->set_flashdata('status', '<div class="alert alert-danger">Error system, silahkan hubungi admin</div>');
                    redirect(base_url().'login/register');
            }else{
                $this->db->trans_commit();
                $this->session->set_flashdata('status', '<div class="alert alert-success">Daftar Berhasil, Silahkan Cek Email Anda. Jika Selama 24 Jam Anda Belum Menerima Email, Mohon Lakukan Pendaftaran Ulang</div>');
                    redirect(base_url().'login');
            }
        } else {
            $this->session->set_flashdata('status', '<div class="alert alert-danger">'.validation_errors().'</div>');
            redirect(base_url().'login/register');
        }
    }
	
	public function verifikasi_email($user = "", $code = "")
    {
		if($user == "" && $code == ""){
			$user = @$_POST['user_full_name'];
			$code = @$_POST['kode_verifikasi'];

        }
        $data['resutl']       = $this->mrg->verifikasi_email($user, $code);

        $cek_user = $this->db->query("SELECT * from user where user_name = '$user' or user_email = '$user' or no_daftar = '$user'")->row();

        $this->session->set_userdata('user_data', $cek_user);
        $log['detail']          = 'Verifikasi '.$user;
                $log['action']  = 'Verifikasi';
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('user_log', $log);

                if ($data) {
                    $this->session->set_flashdata('status', '<div class="alert alert-success">Akun Anda Sudah Di Verifikasi Silahkan login</div>');
                    redirect(base_url().'login');
                } else {
                    $this->session->set_flashdata('status', '<div class="alert alert-danger">Error system, silahkan hubungi admin</div>');
                    redirect(base_url().'login/register');
                }
		die();
        $data['pagetitle']  = 'Verifikasi Page';

        // $image    = @$this->m_global->get('master_image',NULL, ['master_image_aktif' => '1', 'master_image_status' => '1']);
        $image       = ['img1.jpeg','img2.jpeg','img3.jpeg'];

        $data['images'] = json_encode(array("image" => $image ));

        $this->load->view('login/verifikasi', $data);
        
    }
	
    public function verifikasi()
    {

        $data['pagetitle']  = 'Verifikasi Page';

        // $image    = @$this->m_global->get('master_image',NULL, ['master_image_aktif' => '1', 'master_image_status' => '1']);
        $image       = ['img1.jpeg','img2.jpeg','img3.jpeg'];

        $data['images'] = json_encode(array("image" => $image ));

        $this->load->view('login/verifikasi', $data);
        
    }

}
