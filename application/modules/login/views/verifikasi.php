<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo $pagetitle .' - '. $this->config->item( 'app_name' ) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES  -->
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />


    <link href="<?php echo base_url('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') ?> " rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/layouts/layout/css/custom.css') ?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo base_url('assets/global/css/components-md.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url('assets/global/css/plugins-md.min.css') ?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url('assets/pages/css/login-5.css') ?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- BEGIN ICON -->
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- BEGIN BASE URL -->
    <script type="text/javascript">
    	var base_url = "<?php echo base_url(); ?>";
    </script>
</head>

<body class=" login">

    <!-- BEGIN : LOGIN PAGE 5-2 -->
    <div class="user-login-5">
        <div class="row bs-reset">
            <div class="col-md-6 login-container bs-reset">

                <div class="login-content">
                    <!-- <h1><i class="fa fa-cogs"></i> &nbsp;&nbsp;<b>S I M D A</b></h1> -->
                    <img class="login-logo login-6" src="<?php echo base_url('assets/pages/img/'.($this->config->item( 'logo-big-white.png' ) != null ? $this->config->item( 'logo-big-white.png' ) : 'logo-big-white.png') ); ?>" />
                    <p><b> Verifikasi Akun</b></p>
                    <form action="<?=base_url('login/register/verifikasi_email')?>" class="login-form" role="form" method="post">
                        <?php echo $this->session->flashdata('status'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="user_full_name" required/>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="user_password" required/>
                            </div>
                            <div class="col-xs-6">
                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Kode Verifikasi" name="kode_verifikasi" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"><p></p></div>
                            <div class="col-sm-8 text-right">
                                <a class="btn green-default" href="<?=base_url('login')?>">Back</a>
                                <button class="btn green-seagreen" type="submit">Verifikasi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6 bs-reset">
                <div class="login-bg"> </div>
            </div>
        </div>
    </div><!-- END : LOGIN PAGE 5-2 -->

<!-- <script src="<?php echo base_url('assets/global/plugins/respond.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/global/plugins/excanvas.min.js') ?>"></script> -->
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url('assets/global/plugins/jquery.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/backstretch/jquery.backstretch.min.js') ?>" type="text/javascript"></script>
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url('assets/global/scripts/app.min.js') ?>" type="text/javascript"></script>
    <!-- BEGIN IMAGE LOGIN -->
    <script type="text/javascript">
        var img    = <?=$images?>;
        var slide1 = img.image[0];
        var slide2 = img.image[1];
        var slide3 = img.image[2];
    </script>
    <script type="text/javascript">
        $("input#username").on({
            keydown: function(e) {
            if (e.which === 32)
              return false;
            },
            change: function() {
            this.value = this.value.replace(/\s/g, "");
            }
        });
    </script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url('assets/pages/scripts/login-5.js') ?>" type="text/javascript"></script>
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
</body>
</html>
