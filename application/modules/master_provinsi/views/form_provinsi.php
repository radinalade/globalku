<div class="col-md-12">
    <!-- BEGIN VALIDATION STATES-->
    <div class="portlet blue box">

        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
            </div>
            <div class="actions">
            </div>
        </div>

        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <form action="<?= @$url ?>/action_form_provinsi/<?=@$records->kd_provinsi?>" class="form-horizontal form-add" role="form" method="POST">
                <div class="form-body">
                    <div class="alert alert-warning display-hide">
                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                        <span> </span>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="form_control_1">Provinsi
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" required placeholder="Provinsi" value="<?=@$records->nm_provinsi?>" name="nm_provinsi">
                            <span class="help-block"></span>
                        </div>
                    </div>

                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-8">
                            <button type="submit" class="btn blue">Submit</button>
                            <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END VALIDATION STATES-->
</div>
</div>

<a href="<?php echo $url?>" class="ajaxify reload"></a>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.tgl').datepicker({
            format: 'dd-mm-yyyy'
        });
    });
</script>
