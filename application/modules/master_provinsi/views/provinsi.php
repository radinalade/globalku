<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Add" href="<?php echo $url ?>/show_add_provinsi" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?php echo $url ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

<!--                         <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <div class="table-actions-wrapper">
                        <select class="bs-select table-group-action-input form-control input-small" data-style="blue">
                            <option>Select...</option>
                            <option value="99/kd_kec/ref_kecamatan">Delete</option>
                            <option value="0/kd_kec/ref_kecamatan">InActive</option>
                            <option value="1/kd_kec/ref_kecamatan">Active</option>
                        </select>
                        <button class="btn btn-sm btn-icon-only blue table-group-action-submit" onClick="">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

                        <thead>

                            <tr role="row" class="heading">
                                <th width="10px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="20px"> No </th>
                                <th width="20px"> Kode Provinsi </th>
                                <th width="20px"> Nama Provinsi </th>
                                <th width="20px"> Lastupdate </th>
                                <th width="20px"> Action </th>

                            </tr>

                            <tr role="row" class="filter">
                                <td> </td>
                                <td width="20px"> </td>
                                <td width="20px">
                                    <input type="text" class="form-control form-filter input-sm" name="kd_provinsi" placeholder="Kode Provinsi"> </td>
                                <td width="20px">
                                    <input type="text" class="form-control form-filter input-sm" name="nm_provinsi" placeholder="Provinsi" > </td>

                                <td width="20px">
                                    <div class="input-group margin-bottom-5" id="defaultrange">
                                        <input type="text" name="lastupdate_show" class="form-control form-filter">
                                        <input type="hidden" name="lastupdate" class="form-filter">
                                        <span class="input-group-btn">
                                            <button class="btn default date-range-toggle" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>

                                 <td class="text-center" width="20px">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
                                    </div>
                                </td>


                            </tr>

                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                   <div class="row">


    </div>
</div>

                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
    jQuery(document).ready(function() {

        // table data
        var select_url = '<?php echo $url?>/select_provinsi';

        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" }
        ];
        var order = [
            [2, "asc"]
        ];

        var sort = [-1,,1];

        TableDatatablesAjax.handleRecords(select_url, header, order, sort);
        // bs select setelah datatable, bug
        Helper.bsSelect();

    });
</script>
