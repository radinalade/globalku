<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_provinsi extends MX_Controller {

    private $prefix         = 'master_provinsi/menu_provinsi';
    private $url            = 'master_provinsi/menu_provinsi';
    private $table_provinsi = 'provinsi';
    private $table_kota     = 'kota';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	public function index()
    {
        $data['pagetitle']  = 'Provinsi';
        // $data['subtitle']   = 'Page Provinsi';
        $data['url']        = base_url().$this->url;
        $js['js']           = ['table-datatables-ajax'];
        $css['css']         = null;

        $this->template->display('provinsi', $data, $js, $css );
    }

    public function select_provinsi()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_provinsi'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_provinsi' => 'kd_provinsi',
            'nm_provinsi' => 'nm_provinsi',
            'lastupdate'  => 'lastupdate',
            ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";

        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_provinsi, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_provinsi, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_provinsi.'"/><span></span></label>',
                $i,
                $rows->kd_provinsi,
                $rows->nm_provinsi,
                $rows->lastupdate,
                '<a href="'.base_url().$this->url.'/show_kota/'.$rows->kd_provinsi.'" class="ajaxify btn blue-steel btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_provinsi/'.$rows->kd_provinsi.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_provinsi.'/provinsi/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_provinsi.'/provinsi/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_provinsi()
    {
        $data['pagetitle']  = 'Tambah Provinsi';
        // $data['subtitle']   = 'Tambah ';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'form_provinsi', $data, $js );
    }

    public function show_edit_provinsi($id)
    {
        $data['records']    = $this->m_global->get( $this->table_provinsi, null, ['kd_provinsi' => $id] )[0];

        $data['pagetitle']  = 'User';
        $data['subtitle']   = 'manage user';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'form_provinsi', $data, $js );
    }

    public function action_form_provinsi($id = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'provinsi';

        $this->form_validation->set_rules('nm_provinsi', 'nm_provinsi', 'trim');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'nm_provinsi'] = $this->input->post('nm_provinsi');

            if ($id == null) {

                $result  = $this->m_global->insert($this->table_db, $data);

            } else {
                $id = $id[0];
                $result = $this->m_global->update($this->table_db, $data, ['kd_provinsi' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_provinsi').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_provinsi').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function show_kota($id)
    {
        $data['records']    = $this->m_global->get( $this->table_provinsi, null, ['kd_provinsi' => $id] )[0];
        $data['pagetitle']  = 'Kota';
        $data['subtitle']   = '';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('kota', $data, $js, $css );
    }


    public function select_kota($id)
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_kota'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            // 'kd_kecamatan' => 'kd_kec',
            // 'kd_kelurahan' => 'kel_id',
            // 'nm_kelurahan' => 'nm_kel',
            // 'lastupdate'   => 'lastupdate',
        ];

        $where_e = 'kd_kec='.$id;
        $where   = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }


        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_kota, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_kota, status,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_kota, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kel_id.'"/><span></span></label>',
                $i,
                $rows->kd_kota,
                $rows->kd_provinsi,
                $rows->nm_kota,
                $rows->lastupdate,
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_kota/'.$rows->kel_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by_kelurahan/'.$rows->kel_id.'/ref_kelurahan/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by_kelurahan/'.$rows->kel_id.'/ref_kelurahan/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

}

/* End of file Menu_utama.php */
/* Location: ./application/modules/master/controllers/Menu_utama.php */