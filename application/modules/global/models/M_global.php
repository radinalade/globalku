<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_global extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->db->query("SET time_zone='+07:00'");
		$this->session->set_userdata('count_new_user', $this->get_count_new_user());
	}

  

    public function setdateformat($date)
    {

        $date = date('Y-m-d', strtotime($date));
        return $date;
    }

    public function getdateformat($date)
    {
        $date = (@$date == "")?'':date('d-m-Y', strtotime(@$date));
        return $date;
    }
	

	public function get_count_new_user(){
		$count_new_user = $this->db->query("SELECT COUNT(user_id) as TOTAL FROM user WHERE user_wp IS NULL AND user_role = '2'")->row_array();
		return $count_new_user['TOTAL'];
	}

    public function count( $table, $join = NULL, $where = NULL, $where_e = NULL, $group = NULL )
    {
        $this->db->select("count(*) as jumlah")->from($table);

        if ( ! is_null( $join ) )
        {
            foreach ($join as $key => $value) {
                $tipe = ( @$value[2] != '' ) ? $value[2] : 'INNER';
                $this->db->join( $value[0], $value[1], $tipe );
            }
        }

        ( ! is_null($where)
            ? $this->db->where($where)
            : ''
        );

        ( ! is_null($where_e)
            ? $this->db->where($where_e, NULL, FALSE)
            : ''
        );

        ( ! is_null($group)
            ? $this->db->group_by($group, NULL, FALSE)
            : ''
        );

        $query  = $this->db->get();

        // echo $this->db->last_query(); exit;

        $result = $query->row();

        if(empty($result)){
            return '0';
        }
        else{
            return $result->jumlah;
        }
    }

    /**
     * Mengambil data dari $tabel
     *
    **/
    public function get( $table, $join = NULL, $where = NULL, $select = '*', $where_e = NULL, $order = NULL, $start = 0, $tampil = NULL, $group = NULL, $array = null )
    {

        if ( is_array($select)){
            $this->db->select( $select[0], $select[1] )->from($table);
        }
        else{
            $this->db->select($select)->from($table);
        }

        if ( ! is_null( $join ) ){
            foreach ($join as $key => $value) {
                $tipe = ( @$value[2] != '' ) ? $value[2] : 'INNER';
                $this->db->join( $value[0], $value[1], $tipe );
            }
        }

        ( ! is_null( $order )
            ? $this->db->order_by( $order[0], $order[1])
            : ''
        );

        ( ! is_null( $tampil )
            ? $this->db->limit( $tampil, $start)
            : ''
        );

        ( ! is_null( $where )
            ? $this->db->where( $where)
            : ''
        );

        ( ! is_null( $where_e )
            ? $this->db->where( $where_e, NULL, FALSE)
            : ''
        );

        ( ! is_null( $group )
            ? $this->db->group_by( $group, NULL, FALSE)
            : ''
        );

        $query = $this->db->get();

        // echo $this->db->last_query(); exit;
        $err   = $this->db->error();

        if ( $err['code'] != 0 ) {
            pre('lastdb');
            pre($err, 1);
        }

        ( ! is_null( $array )
            ? $result = $query->result_array()
            : $result = $query->result()
        );

        return $result;
    }

    public function get_data_all( $table, $join = NULL, $where = NULL, $select = '*', $where_e = NULL, $order = NULL, $start = 0, $tampil = NULL, $group = NULL, $array = null )
	{
		if ( is_array($select))
		{
			$this->db->select( $select[0], $select[1] )->from($table);
		}
			else
		{
			$this->db->select($select)->from($table);
		}

		if ( ! is_null( $join ) )
		{
			foreach ($join as $key => $value) {
				$tipe = ( @$value[2] != '' ) ? $value[2] : 'INNER';
				$this->db->join( $value[0], $value[1], $tipe );
			}
		}

		( ! is_null( $order )
			? $this->db->order_by( $order[0], $order[1])
			: ''
		);
		( ! is_null( $tampil )
			? $this->db->limit( $tampil, $start)
			: ''
		);
		( ! is_null( $where )
			? $this->db->where( $where)
			: ''
		);
		( ! is_null( $where_e )
			? $this->db->where( $where_e, NULL, FALSE)
			: ''
		);
		( ! is_null( $group )
			? $this->db->group_by( $group, NULL, FALSE)
			: ''
		);

		$query  = $this->db->get();

		( ! is_null( $array )
			? $result = $query->result_array()
			: $result = $query->result()
		);

		return $result;
	}

    public function insert( $table, $data = NULL )
    {
        $result    = $this->db->insert( $table, $data );

        if ( $result == TRUE ){
            $result             = [];
            $result['status']   = TRUE;
            $result['id']       = $this->db->insert_id();
        }
        else{
            $result             = [];
            $result['status']   = FALSE;
        }

        return $result;
    }

    public function update( $table, $data = NULL, $where = NULL, $where_e = NULL )
    {
        ( ! is_null($where_e)
            ? $this->db->where($where_e, NULL, FALSE)
            : ''
        );

        $result = $this->db->update( $table, $data, $where );

        return $result;
    }

    public function delete( $table, $where = NULL, $where_e = NULL )
    {
        ( ! is_null($where_e)
            ? $this->db->where($where_e, NULL, FALSE)
            : ''
        );

        $result = $this->db->delete( $table, $where );

        return $result;
    }

    public function validation( $table, $where, $where_e = NULL )
    {
        $this->db->select('*')->from( $table );

        ( ! is_null($where)
            ? $this->db->where($where)
            : ''
        );
        ( ! is_null($where_e)
            ? $this->db->where($where_e, NULL, FALSE)
            : ''
        );

        $query  = $this->db->get();
        $result = $query->num_rows();

        if( $result > 0 ){
            $result = FALSE;
        }
        else{
            $result = TRUE;
        }

        return $result;
    }

    public function get_num_rows( $table, $join = NULL, $where = NULL, $where_e = NULL, $group = NULL )
    {
        $this->db->select("count(*) as jumlah")->from($table);

        if ( ! is_null( $join ) ){
            foreach ( $join as $rows){
                $tipe = ( @$rows['tipe'] != '' ) ? $rows['tipe'] : 'INNER';
                $this->db->join( $rows['table'], $rows['on'], $tipe );
            }
        }

        ( ! is_null($where)
            ? $this->db->where($where)
            : ''
        );

        ( ! is_null($where_e)
            ? $this->db->where($where_e, NULL, FALSE)
            : ''
        );

        ( ! is_null($group)
            ? $this->db->group_by($group, NULL, FALSE)
            : ''
        );

        $query  = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert_update_batch( $table, $data )
    {
        $count      = 0;
        $jumlah     = 0;

        foreach ($data as $param) {
            $result     = $this->_insert_update($table, $param);
            if ($result == TRUE){
                $count++;
            }
            $jumlah++;
        }

        if ( $count == $jumlah ){
            $result             = [];
            $result['status']   = TRUE;
        }
        else{
            $result             = [];
            $result['status']   = FALSE;
            $result['message']  = ($jumlah - $count) . ' data gagal diproses';
        }

        return $result;
    }

    public function insert_update( $table, $data )
    {
        $result     = $this->_insert_update($table, $data);

        if ( $result == TRUE ){
            $result             = [];
            $result['status']   = TRUE;
        }
        else{
            $result             = [];
            $result['status']   = FALSE;
        }

        return $result;
    }

	public function insert_log($table, $data){
		$key          = [];
        $value        = [];
        //generate duplicate
        $strDuplicate = [];

        foreach ($data as $kolom => $nilai) {
            $key[]          = $kolom;
            $value[]        = $nilai;
            $nilai          = $this->db->escape($nilai);
        }

        // generate tanda tanya
        $tanya  = [];
        foreach ($value as $val){
            $tanya[] = '?';
        }

        $tanya = implode(", ", $tanya);
        $sKey  = implode(",", $key);

        $sql   = "  INSERT INTO {$table}({$sKey}) VALUES ({$tanya});";

        $query  = $this->db->query($sql, $value);

        return $query;
	}

    public function _insert_update($table, $data)
    {
        $key          = [];
        $value        = [];
        //generate duplicate
        $strDuplicate = [];

        foreach ($data as $kolom => $nilai) {
            $key[]          = $kolom;
            $value[]        = $nilai;
            $nilai          = $this->db->escape($nilai);
            $strDuplicate[] = "{$kolom} = {$nilai}";
        }

        $strDuplicate   = implode(",", $strDuplicate);

        // generate tanda tanya
        $tanya  = [];
        foreach ($value as $val){
            $tanya[] = '?';
        }

        $tanya = implode(", ", $tanya);
        $sKey  = implode(",", $key);

        $sql   = "  INSERT INTO {$table}({$sKey}) VALUES ({$tanya})
                    ON DUPLICATE KEY UPDATE
                    {$strDuplicate}
                  ;";

        $query  = $this->db->query($sql, $value);

        return $query;
    }

	public function email($email1, $subject1, $header1, $body1)
	{
		$config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'radinalade15@gmail.com',
            'smtp_pass' => 'manusia97',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('simda@gmail.com', 'Administrator SIMDA');
        $email = str_replace(';', ',', $email1);
        $this->email->to($email);
        //$this->email->bcc('masraga.setiawan@gmail.com');
        $this->email->subject($subject1);
        $body = '<html><body style="background: #ffffff; color: #222; font-family: Arial; margin: 20px; color: #363636; font-size:11px;"><table style="font-family: Arial; border-collapse:collapse;"><tr><td style="width:90px;color: #009900;font-size: 20px;" valign="middle">SIMDA</td><td style="color: #222; padding-left:15px; font-size: 20px; border-left:1px solid;"><b>'.$header1.'</b><div style="color: #888; font-size: 10px;">CONFIDENTIAL</div></td></tr><tr><td colspan="2" height="5"></td></tr></table><div style="height:15px;"></div><table style="background: #efefef; font-size:11px; color: #444; font-family: Arial;"  cellpadding=3 cellspacing=2><tr><td>'.$body1.'</td></tr></table><div style="font-size: 10px; color: #888;"><br><b style="color:#222;">Copyright 2018 - PT. CENDANA IT 2000</div></body></html>';
        $this->email->message($body);
		$chk = $this->email->send();
		if($chk){
			echo "berhasil";
		}else{
			echo $this->email->print_debugger();
			echo "gagal";
		}
        return $chk;
	}

}

/* End of file m_global.php */
/* Location: ./application/modules/global/models/m_global.php */
