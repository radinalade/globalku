<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author https://www.roytuts.com
 */
class Site extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

   public function index()
	{
        $data['pagetitle']  = 'Dashboard';
        $data['subtitle']   = 'Statistics Analysis';
        $js['js']           = null;
        $css['css']         = null;

        $this->template->display($data, $js, $css );
	}

}

/* End of file Site.php */
/* Location: ./application/modules/site/controllers/site.php */