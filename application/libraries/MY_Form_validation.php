<?php

class MY_Form_validation extends CI_Form_validation
{
    function __construct()
	{
	    parent::__construct();
	}

    function run($module = '', $group = '') {
        (is_object($module)) AND $this->CI = &$module;

        return parent::run($group);
    }

    function unique($str, $field)
	{
		$CI =& get_instance();
		list($table, $column) = explode('.', $field, 2);

		$CI->form_validation->set_message('unique', '%s Sudah digunakan.');

		$query = $CI->db->query("SELECT COUNT(*) AS dupe FROM $table WHERE $column = '$str'");
		$row = $query->row();
		return ($row->dupe > 0) ? FALSE : TRUE;
	}

}

?>
