<?php
class Template {

    protected $_ci;

    function __construct()
    {
        $this->_ci = &get_instance();
    }

    function display( $template, $data = NULL, $js = NULL, $css = NULL )
    {
        $url = isset($data['url']) ? $data['url'] : null;

        $status_link    = @$this->_ci->input->post('status_link');
        // if ($this->_check_link($url) === 'false') {
        //     $css['css'] = ['error'];
        //     $template = 'error/error_404';
        // }
            
            $data['_content']       = $this->_ci->load->view($template, $data, TRUE);
            $data['_js']            = $this->_ci->load->view('templates/js', $js, TRUE);
            $data['_css']           = $this->_ci->load->view('templates/css', $css, TRUE);
            $data['_fullcontent']   = $this->_ci->load->view('templates/content', $data, TRUE);
            $data['_header']        = $this->_ci->load->view('templates/header', $data, TRUE);
            $data['_sidebar']       = $this->_ci->load->view('templates/sidebar', $data, TRUE);
            $data['_footer']        = $this->_ci->load->view('templates/footer', $data, TRUE);
            


            $this->_ci->load->view('templates/template.php', $data);

        
    }
 }



?>
