<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['packages'] = array();


$autoload['libraries'] = array('template','database','form_validation','session', 'bcrypt');


$autoload['drivers'] = array();


$autoload['helper'] = array('url', 'form', 'auth', 'menu', 'cdn', 'get_file');


$autoload['config'] = array();


$autoload['language'] = array();



$autoload['model'] = array('global/m_global', 'global/m_global_new');
