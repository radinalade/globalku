<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

# untuk print_f
function pre( $var, $exit = null )
{
    $CI = &get_instance();
    echo '<pre>';
    if ( $var == 'lastdb' ){
        print_r($CI->db->last_query());
    }
    else if ( $var == 'post' ){
        print_r($CI->input->post());
    }
    else if ( $var == 'get' ){
        print_r($CI->input->get());
    }
    else {
        print_r( $var );
    }

    echo '</pre>';

    if ( $exit ){
        exit();
    }
}


function md5_mod($str, $salt){

    $str = md5(md5($str).$salt);

    return $str;
}


function login_data( $val )
{
    $CI        = &get_instance();
    $user_data = $CI->session->userdata('user_data');

    return $user_data->$val;
}


function uang( $var, $tipe=null, $dec="0" )
{
    if ( empty($var) ) return 0;

    return 'Rp. ' . number_format(str_replace(',','.',$var), $dec,',','.').($dec=="0"?($tipe == true ? ',-' : ",00" ):'');
}


function uang2( $var, $dec="0" )
{
    if ( empty($var) ) return 0;

    return number_format(str_replace(',','.',$var),$dec,',','.');
}


function bulan($bulan)
{
    $aBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    return $aBulan[$bulan];
}


function hari($hari)
{
    $data  = date('N', strtotime($hari));
    $aHari = ['', 'Senin', 'Selasa','Rabu','Kamis',"Jum'at",'Sabtu','Minggu'];

    return $aHari[$data];
}


function tgl_format($tgl , $format = "")
{
    if ($format == "") {
        $tanggal    = date('d', strtotime($tgl));
        $bulan      = bulan( date('n', strtotime($tgl))-1 );
        $tahun      = date('Y', strtotime($tgl));

        return $tanggal.' '.$bulan.' '.$tahun;
    }
    elseif ($format == 'd/m/Y') {
        $tanggal    = date('d', strtotime($tgl));
        $bulan      = date('m',strtotime($tgl));
        $tahun      = date('Y', strtotime($tgl));

        return $tanggal.'-'.$bulan.'-'.$tahun;
    }
}


function date_format_indo($tgl,$style=null)
{
    $exp     = explode(' ', $tgl);
    $detik   = date('s', strtotime($tgl));
    $menit   = date('i', strtotime($tgl));
    $jam     = date('H', strtotime($tgl));
    $tanggal = date('d', strtotime($tgl));
    $bulan   = bulan( date('n', strtotime($tgl))-1 );
    $tahun   = date('Y', strtotime($tgl));

    if( empty($exp[1]) ){
        return $tanggal.' '.$bulan.' '.$tahun;
    }
    else{
        if($style == true){
            return $tanggal.' '.$bulan.' '.$tahun.' '.$jam.':'.$menit.':'.$detik;
        }
        else{
            return $tanggal.' '.$bulan.' '.$tahun;
        }
    }
}


function bulan_tahun($param){
    $bulan = bulan( date('n', strtotime($param))-1 );
    $tahun = date('Y', strtotime($param));

    return $bulan.' '.$tahun;
}


function date_value($tgl)
{
   $date      = date_format_indo($tgl);
   $date_indo = explode(' ', $date);

   $hr        = hari($tgl);
   $tg        = Terbilang($date_indo[0]);
   $bln       = $date_indo[1];
   $thn       = Terbilang($date_indo[2]);

   return $hr . ' tanggal ' . $tg . ' bulan ' . $bln . ' tahun ' . $thn;
}


function bln_thn($tgl, $deli)
{
    $data   = explode($deli, $tgl);
    $x      = (intval($data[0])-1);

    return bulan($x) . ' ' . $data[1];
}


function config( $val, $json = null )
{
    $CI     = & get_instance();
    $result = $CI->m_global->get_data_all('config', null, ['config_nama' => $val])[0];

    if ( $result ) {
        if ( $json ){
            return json_decode( $result->config_value );
        }
        else {
            return $result->config_value;
        }
    }
    else {
        return null;
    }
}


function terbilang( $x )
{
    $x     = abs($x);
    $angka = array("", "satu ", "dua ", "tiga ", "empat ", "lima ","enam ", "tujuh ", "delapan ", "sembilan ", "sepuluh ", "sebelas ");
    $temp  = "";

    if ($x <12) {
        $temp = "". $angka[$x];
    }
    else if ($x <20) {
        $temp = Terbilang($x - 10). "belas ";
    }
    else if ($x <100) {
        $temp = Terbilang($x/10)."puluh ". Terbilang($x % 10);
    }
    else if ($x <200) {
        $temp = "seratus " . Terbilang($x - 100);
    }
    else if ($x <1000) {
        $temp = Terbilang($x/100) . "ratus " . Terbilang($x % 100);
    }
    else if ($x <2000) {
        $temp = " seribu" . Terbilang($x - 1000);
    }
    else if ($x <1000000) {
        $temp = Terbilang($x/1000) . "ribu " . Terbilang($x % 1000);
    }
    else if ($x <1000000000) {
        $temp = Terbilang($x/1000000) . "juta " . Terbilang($x % 1000000);
    }
    else if ($x <1000000000000) {
        $temp = Terbilang($x/1000000000) . "milyar " . Terbilang(fmod($x,1000000000));
    }
    else if ($x <1000000000000000) {
        $temp = Terbilang($x/1000000000000) . "trilyun " . Terbilang(fmod($x,1000000000000));
    }

    return $temp;
}


function list_name($array)
{
    $data   = '';
    $count  = count($array);

    if($count == 1) {
        $data = $array[0];
    }
    else if ($count == 2) {
        $data = $array[0] . ' dan ' . $array[1];
    }
    else if ($count > 2) {
        foreach ($array as $key => $val) {
            ($key == ($count - 1)) ?
            $data .= ' dan ' . $val :
            $data .= $val . ', ';
        }
    }

    return $data;
}


function get_select2( $tabel, $prefix, $val )
{
    if ( count($val) > 0 )
    {
        $CI = &get_instance();
        $result = $CI->m_global->get_data_all( $tabel, null, null, '*', $prefix.'_id IN ('.$val.')' );

        return $result;
    } else {
        return [];
    };

}

function sidebar_menu( $menu, $url )
{
    foreach ( $menu as $key => $value )
    {
    	echo ( @$value['header'] != '' ? '
            <li class="heading">
                <h3 class="uppercase">'.$value['header'].'</h3>
            </li>' : '' );

    	echo '<li class="' .
            /*
                Jika nama path dari menu helper sama dengan path
            */
            ( $value['path'] == $url
                ? 'nav-item active open'
                : 'nav-item'
            ).'" >

            <a ' .
            /*
                Mempunyai sub menu atau tidak
                untuk link href
            */
            (is_array( $value['link'] )
                ? 'href="javascript:;" class="nav-link nav-toggle"'
                : 'class="nav-link ajaxify" href="'.base_url($value['link']).'"') .
            '>

            <i class="icon-'.$value['icon'].'"></i>

            <span class="title">'.$value['name'].'</span>' .

            /*
                Mempunyai sedang aktif
            */
            ($key == 0
                ? '<span class="selected"></span>'
                : ''
            ) .

            /*
                Mempunyai sub menu atau tidak
                untuk menampilkan arrow
            */
            (is_array($value['link'])
                ? '<span class="arrow ' .
                    ( $value['path'] == $url
                    ? 'open'
                    : '')
                . '"></span>'
                : ''
            ) . '</a>';

            sub_menu( $value, $url, '2' );

        echo '</li>';
    }
}


function sub_menu( $value, $url, $segment ){

    /*
        Mempunyai sub menu atau tidak
        untuk menampilkan sub link
    */

    if ( is_array($value['link']) )
    {
        echo '<ul class="sub-menu">';

        $CI =& get_instance();

        /*
            Menampilkan sub menu
        */

        foreach ( $value['link'] as $kSub => $kValue )
        {
            $sub_url = $CI->uri->segment($segment);

            /*
                Jika path parent sama dengan uri sebelumnya
                dan path sekarang sama dengan uri sekarang
            */

            echo '<li class="nav-item ' .
                ($kValue['path'] == $sub_url && $value['path'] == $url
                    ? 'active open'
                    : ''
                ) . '" >

                <a ' .

                /*
                    Jika mempunyai sub, maka href=javascript (tidak ada link)
                    jika tidak, maka href berisi link
                */

                (is_array($kValue['link'])
                    ? 'href="javascript:;" class="nav-link" '
                    : 'class="nav-link ajaxify" href="'.base_url($kValue['link']).'"'
                ) .

                '>
                    <i class="icon-'.$kValue['icon'].'"></i>
                    ' . $kValue['name'] .

                /*
                    Jika mempunyai sub dan path parent sama dengan uri sekarang
                    maka arrow open (sub menu sedang aktif)
                    selain itu, hanya menampilkan arrow (mempunyai sub menu tapi tidak aktif)
                */

                ( $kValue['path'] == $sub_url
                    ? '<span class="selected"></span>'
                    : ''
                ) .

                (is_array($kValue['link'])
                    ? '<span class="arrow ' .
                        ( $value['path'] == $url
                        ? 'open'
                        : '')
                    . '"></span>'
                    : ''
                ) . '</a>';

                /*
                    cek lagi gan sub menu level selanjutnya
                */

                sub_menu( $kValue, $sub_url, $segment+1 );

             echo '</li>';
        }
        echo '</ul>';
    }
}

?>
