<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * [Fungsi untuk generate menu]
 * @return [array] [Array menu]
 */

function _getUserAccess(){
    $CI =& get_instance();

    $get['table']   = 'user';
    $get['join']    = [['roles', 'role_id = user_role']];
    $get['where']   = ['user_id' => login_data('user_id')];
    $get['select']  = 'role_access';

    $result = $CI->m_global_new->get($get)[0]->role_access;

    return $result;
}

function menu2($all = '')
{
    $CI =& get_instance();

    $result     = [];
    $where_e    = ($all === '') ? '`menu_id` IN ('._getUserAccess().')' : null ;

    $get['table']   = 'menus';
    $get['where']   = ['menu_status' => '1', 'menu_parent' => '0'];
    $get['where_e'] = $where_e;
    $get['order']   = ['menu_number', 'ASC'];

    $menu_db    = $CI->m_global_new->get($get);

    foreach ($menu_db as $key => $val) {
        $result[] = [
                        'id'   => $val->menu_id,
                        'name' => $val->menu_name,
                        'link' => _linkDB($val, $all),
                        'icon' => $val->menu_icon,
                    ];
    }

    return $result;
}

function _linkDB($val, $all)
{
    $CI         =& get_instance();

    $where_e    = ($all === '') ? '`menu_id` IN ('._getUserAccess().')' : null ;

    $get['table']   = 'menus';
    $get['where']   = ['menu_status' => '1', 'menu_parent' => $val->menu_id];
    $get['where_e'] = $where_e;
    $get['order']   = ['menu_number', 'ASC'];

    $link = $CI->m_global_new->get($get);

    $resultLink = [];

    if($link) {
        foreach ($link as $k => $v) {
            $resultLink[] = [
                                'id'   => $v->menu_id,
                                'name' => $v->menu_name,
                                'link' => _linkDB($v, $all),
                                'icon' => $v->menu_icon,
                            ];
        }
    } else {
        $resultLink = $val->menu_link;
    }

    return $resultLink;
}

function v_menu($menu, $url)
{
    if (!isset($url[1])) {
        $url[1] = 'dashboard';
    }

    foreach ($menu as $key => $val) {
        $link   = 'javascript:;';
        $span   = '<span class="arrow"></span>';
        $class  = 'nav-link nav-toggle';

        if(!is_array($val['link'])) {
            $link   = base_url($val['link']);
            $span   = '';
            // $class  = 'nav-link nav-toggle';
            $class  = 'ajaxify nav-link nav-toggle';
        }

        echo '<li class="nav-item">
                <a href="'.$link.'" class="'.($val['name'] == 'Dashboard' ? '' : $class).'">
                    <i class="icon-'.$val['icon'].'"></i>
                    <span class="title">'.$val['name'].'</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>'.
                (is_array($val['link']) ?
                '<ul class="sub-menu">'.
                    _submenu($val['link'], $url).
                '</ul>'
                : '').
            '</li>';

    }
}

function _submenu($val, $url)
{
    $result = '';

    foreach ($val as $k => $v) {
        $link   = 'javascript:;';
        $span   = '<span class="arrow"></span>';
        $class  = 'nav-link nav-toggle';

        if(!is_array($v['link'])) {
            $link   = base_url($v['link']);
            $span   = '';
            // $class  = 'nav-link nav-toggle';
            $class  = 'ajaxify nav-link nav-toggle';
        }

        $result .= '<li class="nav-item">
                        <a href="'.$link.'" class="'.$class.'">
                            <i class="icon-'.$v['icon'].'"></i>
                            <span class="title">'.$v['name'].'</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>'.
                        (is_array($v['link']) ?
                        '<ul class="sub-menu">'.
                            _submenu($v['link'], $url).
                        '</ul>'
                        : '').
                    '</li>';

    }

    return $result;
}

/*========================
// tree
==========================*/
function v_tree_view($menu, $access = null) {
	$data_access = ['1'];

	if ($access) {
		$data_access = explode(', ', $access);
	}

	$html = '<ul id="rolesData" style="display: none;">';
	foreach ($menu as $key => $val) {
		$selected = (in_array($val['id'], $data_access)) ? 'selected' : '' ;

		$exp = (is_array($val['link'])) ? 'expanded' : '';

		$html .=  '<li data-id="'.$val['id'].'" class="folder '.$selected.' '.$exp.'">'
		.$val['name']
		. (is_array($val['link']) ?
			'<ul>' .
			_tree_submenu($val['link'], $data_access)
			. '</ul>'
			: ''
		) . '
		</li>';
	}

	$html .= '</ul>';

	echo $html;
}

function _tree_submenu($val, $access = null)
{
	$html = '';
	foreach ($val as $k => $v) {
		$selected = (in_array($v['id'], $access)) ? 'selected' : '' ;
		$exp = (is_array($v['link'])) ? 'expanded' : '';

		$html .=  '<li data-id="'.$v['id'].'" class="folder '.$selected.' '.$exp.'">'
		.$v['name']
		. (is_array($v['link']) ?
			'<ul>' .
			_tree_submenu($v['link'], $access)
			. '</ul>'
			: ''
		) . '
		</li>';
	}

	return $html;
}

function v_list_menu($menu)
{
	$html = '<div class="dd" id="nestable_list">';
		$html .= '<ol class="dd-list">';
			foreach ($menu as $key => $val) {
				$html .= '<li class="dd-item" data-id="'.$val['id'].'">'
					.'<div class="dd-handle" style="border-radius: 0px;"> '.$val['name'].' </div>'
					. (is_array($val['link']) ?
						'<ol class="dd-list">' .
							_list_submenu($val['link'], $val['id'])
						. '</ol>'
						: ''
					) . '
					</li>';
			}
		$html .= '</ol>';
	$html .= '</div>';

	echo $html;
}

function _list_submenu($val, $id)
{
	$html = '';
	foreach ($val as $k => $v) {
		$html .= '<li class="dd-item" data-id="'.$v['id'].'">'
			.'<div class="dd-handle" style="border-radius: 0px;"> '.$v['name'].' </div>'
			. (is_array($v['link']) ?
				'<ol class="dd-list">' .
					_list_submenu($v['link'], $id)
				. '</ol>'
				: ''
			) . '
		</li>';
	}

	return $html;
}

/*========================
// menu static
==========================*/
function menu()
{
    $CI      =& get_instance();

    $dashboard      = ['name' => 'Dashboard',           'link' => 'dashboard',      'icon' => 'screen-desktop',     'path' => 'dashboard'];
    $landing        = ['name' => 'Landing Page',        'link' => 'landing',        'icon' => 'screen-desktop',     'path' => 'landing'];
    $user      	    = ['name' => 'User',                'link' => 'user',           'icon' => 'user',               'path' => 'user'];
    $level3      	= ['name' => 'Level 3',             'link' => 'link3',          'icon' => 'link',               'path' => 'link3'];
    $level2      	= ['name' => 'Level 2',             'link' => 'link1/link2',    'icon' => 'link',               'path' => 'link2'];
    $level1         = ['name' => 'Level 1',             'link' => [$level2, $level3], 'icon' => 'link',             'path' => 'link1'];
    $level4      	= ['name' => 'Level 1',             'link' => [$level2, $level3], 'icon' => 'link',             'path' => 'link1'];

    $menu           = [ $landing, $dashboard, $user, $level1, $level4 ];

	return $menu;

}

// // ==================================================================================
// /**
//  * [Fungsi untuk generate menu]
//  * @return [array] [Array menu]
//  */
//
// //  function _linkDB($val, $all)
// //  {
// //      $CI         =& get_instance();
// //
// //  	$where_e 	= ($all === '') ? '`id` IN (1,2,3,4,5,6)' : null ;
// //  	$link = $CI->m_global->get_data_all('menu', null, ['parent_id' => $val->id], '*', $where_e);
// //
// //  	$resultLink = [];
// //
// //  	if($link) {
// //  		foreach ($link as $k => $v) {
// //  			$resultLink[] = [
// //  								'id'   => $v->id,
// //  								'name' => $v->name,
// //  								'link' => _linkDB($v, $all),
// //  								'icon' => $v->icon,
// //                                 'path' => $val->path,
// //  							];
// //  		}
// //  	} else {
// //  		$resultLink = $val->link;
// //  	}
// //
// //  	return $resultLink;
// // }
// //
// function menu($all = '')
// {
// //     $CI =& get_instance();
// //
// // 	$result 	= [];
// // 	// $where_e 	= ($all === '') ? '`menu_id` IN ('._getUserAccess().')' : null ;
// // 	$where_e 	= ($all === '') ? '`id` IN (1,2,3,4,5,6)' : null ;
// //
// // 	$menu_db	= $CI->m_global->get_data_all('menu', null, ['parent_id' => '0'], '*', $where_e);
// //
// // 	foreach ($menu_db as $key => $val) {
// // 		$result[] = [
// // 						'id'   => $val->id,
// // 						'name' => $val->name,
// // 						'link' => _linkDB($val, $all),
// // 						'icon' => $val->icon,
// // 						'path' => $val->path,
// // 					];
// // 	}
// //     // echo "<pre>",print_r($result),exit();
// // 	return $result;
//
//
//     // menu pendaftaran
//     $pendaftaran_pajak                          = [ 'name' => 'Pendaftaran Pajak',         'link' => 'pendaftaran/pendaftaran_pajak',         'icon' => 'doc',     'path' => 'Pendaftaran Pajak' ];
//     $pendaftaran_retribusi                      = [ 'name' => 'Pendaftaran Retribusi',     'link' => 'pendaftaran/pendaftaran_retribusi',     'icon' => 'doc',     'path' => 'Pendaftaran Retribusi' ];
//     // menu utama
//     $de_pendaftaran                             = [ 'name' => 'Pendaftaran',  'link' => [$pendaftaran_pajak, $pendaftaran_retribusi],  'icon' => 'note',  'path' => 'Pendaftaran' ];
//
//     // echo "<pre>",print_r($query),exit();
//
//     //menu pendataan => pajak sptpd
//     $de_pendat_paj_sptpd                        = [ 'name' => 'SPTPD',                'link' => 'pendataan/pendataan_pajak_sptpd',  'icon' => 'doc',  'path' => 'SPTPD' ];
//     //menu pendataan => pajak teguran
//     $de_pendat_paj_teguran_sptpd                = [ 'name' => 'Teguran SPTPD',         'link' => 'pendataan/teguran_pajak_sptpd',   'icon' => 'doc',  'path' => 'SPTPD' ];
//     //menu pendataan => wajib pajak
//     $de_pendat_paj_wp                           = [ 'name' => 'Wajib Pajak (WP)',     'link' => "pendataan/pendataan_pajak_wp",                             'icon' => 'doc',     'path' => 'Wajib Pajak (WP)' ];
//     //menu pendataan => retribusi
//     $de_pendat_ret_wr                           = [ 'name' => 'Wajib Retribusi',      'link' => 'pendataan/Pendataan_retribusi_wr',                         'icon' => 'doc',     'path' => 'Wajib Retribusi' ];
//     $de_pendat_ret_sptrd                        = [ 'name' => 'SPTRD',                'link' => 'pendataan/Pendataan_retribusi_sptrd',                      'icon' => 'doc',     'path' => 'SPTRD' ];
//     //menu pendataan
//     $de_pendat_pajak                            = [ 'name' => 'Pajak',      'link' => [$de_pendat_paj_wp, $de_pendat_paj_sptpd, $de_pendat_paj_teguran_sptpd],  'icon' => 'wallet',     'path' => 'Pajak' ];
//     $de_pendat_retribusi                        = [ 'name' => 'Retribusi',  'link' => [$de_pendat_ret_wr, $de_pendat_ret_sptrd],                            'icon' => 'wallet',  'path' => 'Retribusi' ];
//     // menu utama
//     $de_pendataan                               = [ 'name' => 'Pendataan',  'link' => [$de_pendat_pajak, $de_pendat_retribusi],  'icon' => 'docs',  'path' => 'Pendataan' ];
//
//
//     //menu penetapan => pajak
//     $de_penet_paj_nota_perhitungan              = [ 'name' => 'Nota Perhitungan',     'link' => 'penetapan/Penetapan_pajak_np',           'icon' => 'calculator',     'path' => 'Nota Perhitungan' ];
//     $de_penet_paj_skpd                          = [ 'name' => 'SKPD',                 'link' => 'penetapan/Penetapan_pajak_skpd',         'icon' => 'doc',            'path' => 'SKPD' ];
//     //menu penetapan => retribusi
//     $de_penet_ret_skrd                          = [ 'name' => 'SKRD',                 'link' => 'penetapan/Penetapan_retribusi_skrd',     'icon' => 'doc',            'path' => 'SKRD' ];
//     //menu penetapan
//     $de_penet_pajak                             = [ 'name' => 'Pajak',      'link' => [$de_penet_paj_nota_perhitungan, $de_penet_paj_skpd],    'icon' => 'wallet',     'path' => 'Pajak' ];
//     $de_penet_retribusi                         = [ 'name' => 'Retribusi',  'link' => [$de_penet_ret_skrd],                                    'icon' => 'wallet',     'path' => 'Retribusi' ];
//     // menu utama
//     $de_penetapan                               = [ 'name' => 'Penetapan',  'link' => [$de_penet_pajak, $de_penet_retribusi],  'icon' => 'paper-plane',  'path' => 'Penetapan' ];
//
//
//     //menu penagihan => pajak
//     $de_penag_paj_stpd                          = [ 'name' => 'STPD',          'link' => 'penagihan/penagihan_pajak_stpd',        'icon' => 'doc',     'path' => 'STPD' ];
//     $de_penag_paj_keberatan                     = [ 'name' => 'Keberatan',     'link' => 'penagihan/penagihan_pajak_keberatan',   'icon' => 'doc',     'path' => 'Keberatan' ];
//     $de_penag_paj_angsuran                      = [ 'name' => 'Angsuran',      'link' => 'penagihan/penagihan_pajak_angsuran',    'icon' => 'doc',     'path' => 'Angsuran' ];
//     //menu penagihan => retribusi
//     $de_penag_ret_strd                          = [ 'name' => 'STRD',          'link' => 'penagihan/penagihan_retribusi_strd',    'icon' => 'doc',     'path' => 'STRD' ];
//     //menu penagihan
//     $de_penag_pajak                             = [ 'name' => 'Pajak',      'link' => [$de_penag_paj_stpd, $de_penag_paj_keberatan, $de_penag_paj_angsuran],  'icon' => 'wallet',     'path' => 'Pajak' ];
//     $de_penag_retribusi                         = [ 'name' => 'Retribusi',  'link' => [$de_penag_ret_strd],                                                   'icon' => 'wallet',     'path' => 'Retribusi' ];
//     // menu utama
//     $de_penagihan                               = [ 'name' => 'Penagihan',  'link' => [$de_penag_pajak, $de_penag_retribusi],  'icon' => 'info',  'path' => 'Penagihan' ];
//
//
//     //menu bendahara penerimaan => pajak
//     $de_bend_paj_sspd_masa                      = [ 'name' => 'SSPD Masa',             'link' => 'bendahara_penerimaan/penerimaan_pajak_sspd',                              'icon' => 'doc',     'path' => 'SSPD Masa' ];
//     $de_bend_paj_sspd_kur_bayar                 = [ 'name' => 'SSPD Kurang Bayar',     'link' => 'bendahara_penerimaan/penerimaan_pajak_sspd_kurang_bayar',      'icon' => 'wallet',     'path' => 'SSPD Kurang Bayar' ];
//     $de_bend_paj_sspd_sanksi                    = [ 'name' => 'SSPD Sanksi',           'link' => 'bendahara_penerimaan/penerimaan_pajak_sspd_sanksi',            'icon' => 'wallet',     'path' => 'SSPD Sanksi' ];
//     $de_bend_paj_bukti_penerimaan               = [ 'name' => 'Bukti Penerimaan',      'link' => 'bendahara_penerimaan/penerimaan_pajak_bukti_penerimaan',       'icon' => 'wallet',     'path' => 'Bukti Penerimaan' ];
//     $de_bend_paj_sts                            = [ 'name' => 'Surat Tanda Setoran',   'link' => 'bendahara_penerimaan/penerimaan_pajak_sts',                    'icon' => 'wallet',     'path' => 'Surat Tanda Setoran (STS)' ];
//     //menu bendahara penerimaan => retribusi
//     $de_bend_ret_ssrd_masa                      = [ 'name' => 'SSRD Masa',             'link' => 'bendahara_penerimaan/penerimaan_retribusi_ssrd_masa',          'icon' => 'wallet',     'path' => 'SSRD Masa' ];
//     $de_bend_ret_ssrd_kur_bayar                 = [ 'name' => 'SSRD Kurang Bayar',     'link' => 'bendahara_penerimaan/penerimaan_retribusi_ssrd_kurang_bayar',  'icon' => 'wallet',     'path' => 'SSRD Kurang Bayar' ];
//     $de_bend_ret_ssrd_sanksi                    = [ 'name' => 'SSRD Sanksi',           'link' => 'bendahara_penerimaan/penerimaan_retribusi_ssrd_sanksi',        'icon' => 'wallet',     'path' => 'SSRD Sanksi' ];
//     $de_bend_ret_bukti_penerimaan               = [ 'name' => 'Bukti Penerimaan',      'link' => 'bendahara_penerimaan/penerimaan_retribusi_bukti_penerimaan',   'icon' => 'wallet',     'path' => 'Bukti Penerimaan' ];
//     $de_bend_ret_sts                            = [ 'name' => 'Surat Tanda Setoran',   'link' => 'bendahara_penerimaan/penerimaan_retribusi_sts',                'icon' => 'wallet',     'path' => 'Surat Tanda Setoran (STS)' ];
//     //menu bendahara penerimaan => lain-lain
//     $de_bend_lain_bukti_penerimaan              = [ 'name' => 'Bukti Penerimaan',      'link' => 'bendahara_penerimaan/penerimaan_lain_lain_bukti_penerimaan',   'icon' => 'wallet',     'path' => 'Bukti Penerimaan' ];
//     $de_bend_lain_sts                           = [ 'name' => 'Surat Tanda Setoran',   'link' => 'bendahara_penerimaan/penerimaan_lain_lain_sts',                'icon' => 'wallet',     'path' => 'Surat Tanda Setoran (STS)' ];
//     //menu bendahara penerimaan
//     $de_bend_pajak                              = [ 'name' => 'Pajak',      'link' => [$de_bend_paj_sspd_masa, $de_bend_paj_sspd_kur_bayar, $de_bend_paj_sspd_sanksi, $de_bend_paj_bukti_penerimaan, $de_bend_paj_sts],     'icon' => 'wallet',     'path' => 'Pajak' ];
//     $de_bend_retribusi                          = [ 'name' => 'Retribusi',  'link' => [$de_bend_ret_ssrd_masa, $de_bend_ret_ssrd_kur_bayar, $de_bend_ret_ssrd_sanksi, $de_bend_ret_bukti_penerimaan, $de_bend_ret_sts],     'icon' => 'wallet',     'path' => 'Retribusi' ];
//     $de_bend_lain                               = [ 'name' => 'Lain-lain',  'link' => [$de_bend_lain_bukti_penerimaan, $de_bend_lain_sts],                                                                                  'icon' => 'wallet',     'path' => 'Lain-Lain' ];
//     // menu utama
//     $de_bendahara_penerimaan                    = [ 'name' => 'Bendahara Penerimaan',  'link' => [$de_bend_pajak, $de_bend_retribusi, $de_bend_lain],  'icon' => 'drawer',  'path' => 'Bendahara Penerimaan' ];
//
//
//     //menu pembukuan dan pelaporan => saldo awal piutang
//     $de_pem_saldo_awal_piu_pajak                = [ 'name' => 'Pajak',                         'link' => 'pembukuan/pembukuan_saldo_piutang_awal_pajak',                     'icon' => 'doc',              'path' => 'Pajak' ];
//     $de_pem_saldo_awal_piu_retribusi            = [ 'name' => 'Retribusi',                     'link' => 'pembukuan/pembukuan_saldo_piutang_awal_retribusi',                 'icon' => 'doc',              'path' => 'Retribusi' ];
//     //menu pembukuan dan pelaporan => proses saldo piutang akhir
//     $de_pem_pspa_pajak                          = [ 'name' => 'Pajak',                         'link' => 'pembukuan/pembukuan_saldo_piutang_akhir_pajak',                    'icon' => 'doc',              'path' => 'Pajak' ];
//     $de_pem_pspa_retribusi                      = [ 'name' => 'Retribusi',                     'link' => 'pembukuan/pembukuan_saldo_piutang_akhir_retribusi',                'icon' => 'doc',              'path' => 'Retribusi' ];
//     //menu pembukuan dan pelaporan => validasi saldo piutang akhir
//     $de_pem_vspa_pajak                          = [ 'name' => 'Pajak',                         'link' => 'pembukuan/pembukuan_validasi_piutang_akhir_pajak',                 'icon' => 'doc',              'path' => 'Pajak' ];
//     $de_pem_vspa_retribusi                      = [ 'name' => 'Retribusi',                     'link' => 'pembukuan/pembukuan_validasi_piutang_akhir_retribusi',             'icon' => 'doc',              'path' => 'Retribusi' ];
//     //menu pembukuan dan pelaporan
//     $de_pem_anggaran                            = [ 'name' => 'Anggaran',                      'link' => 'pembukuan/pembukuan_anggaran',                                     'icon' => 'doc',              'path' => 'Anggaran' ];
//     $de_pem_sspd                                = [ 'name' => 'Validasi SSPD',                 'link' => 'pembukuan/pembukuan_sspd',                                             'icon' => 'doc',              'path' => 'Validasi SSPD' ];
//     $de_pem_saldo_awal_kas                      = [ 'name' => 'Saldo Awal Kas',                'link' => 'pembukuan/pembukuan_saldo_awal_kas',                               'icon' => 'doc',              'path' => 'Saldo Awal Kas' ];
//     $de_pem_sts                                 = [ 'name' => 'Surat Tanda Setoran',           'link' => 'pembukuan/pembukuan_sts',                                          'icon' => 'doc',              'path' => 'Surat Tanda Setoran (STS)' ];
//     $de_pem_saldo_awal_piutang                  = [ 'name' => 'Saldo Piutang Awal',            'link' => [$de_pem_saldo_awal_piu_pajak, $de_pem_saldo_awal_piu_retribusi],   'icon' => 'wallet',           'path' => 'Saldo Awal Piutang' ];
//     $de_pem_pspa                                = [ 'name' => 'Saldo Piutang Akhir',           'link' => [$de_pem_pspa_pajak, $de_pem_pspa_retribusi],                       'icon' => 'wallet',           'path' => 'Proses Saldo Piutang Akhir' ];
//     $de_pem_vspa                                = [ 'name' => 'Validasi Piutang Akhir',        'link' => [$de_pem_vspa_pajak, $de_pem_vspa_retribusi],                       'icon' => 'wallet',           'path' => 'Validasi Saldo Piutang Akhir' ];
//     // menu utama
//     $de_pembukuan_dan_pelaporan                 = [ 'name' => 'Pembukuan & Pelaporan',  'link' => [$de_pem_anggaran, $de_pem_sspd, $de_pem_saldo_awal_kas, $de_pem_saldo_awal_piutang, $de_pem_pspa, $de_pem_vspa, $de_pem_sts],  'icon' => 'notebook',  'path' => 'Pembukuan Dan Pelaporan' ];
//
//
//     //menu parameter
//     $data_umum_pemda                            = [ 'name' => 'Data Umum Pemda',         'link' => 'parameter/data_umum_pemda',            'icon' => 'info',         'path' => 'Data Umum Pemda' ];
//     $unit_organisasi                            = [ 'name' => 'Unit Organisasi',         'link' => 'parameter/unit_organisasi',            'icon' => 'users',        'path' => 'Unit Organisasi' ];
//     $data_umum_organisasi                       = [ 'name' => 'Data Umum Organisasi',    'link' => 'parameter/data_umum_organisasi',       'icon' => 'users',        'path' => 'Data Umum Organisasi' ];
//     $kat_penyisihan                             = [ 'name'  => 'Kategori Penyisihan',    'link' => 'parameter/kategori_penyisihan',        'icon' => 'tag',          'path' => 'Kategori Penyisihan' ];
//     $penyisihan                                 = [ 'name' => 'Penyisihan',              'link' => 'parameter/perhitungan_penyisihan',     'icon' => 'list',         'path' => 'Penyisihan' ];
//     $bphtb                                      = [ 'name' => 'BPHTB',                   'link' => 'parameter/bphtb',                      'icon' => 'map',         'path' => 'BPHTB' ];
//     $perhitungan_sanksi                         = [ 'name' => 'Perhitungan Sanksi',      'link' => 'parameter/perhitungan_sanksi_pajak',   'icon' => 'calculator',   'path' => 'Perhitungan Sanksi' ];
//     $perhitungan_pajak                          = [ 'name' => 'Perhitungan Pajak',       'link' => 'parameter/perhitungan_pajak',          'icon' => 'calculator',   'path' => 'Perhitungan Pajak' ];
//     $perhitungan_penyisihan                     = [ 'name' => 'Penyisihan',              'link' => [$kat_penyisihan, $penyisihan],         'icon' => 'calculator',   'path' => 'Perhitungan Penyisihan' ];
//     $bank                                       = [ 'name' => 'Bank',                    'link' => 'parameter/bank',                       'icon' => 'wallet',       'path' => 'Bank' ];
//     $kecamatan_kelurahan                        = [ 'name' => 'Kecamatan Kelurahan',     'link' => 'parameter/kecamatan_kelurahan',        'icon' => 'directions',   'path' => 'Kecamatan Kelurahan' ];
//     $mapping_rekening_retribusi                 = [ 'name' => 'Mapping Rek Retribusi',   'link' => 'parameter/mapping_rekening_retribusi', 'icon' => 'credit-card',  'path' => 'Mapping Rekening Retribusi' ];
//     $mapping_rekening_pajak                     = [ 'name' => 'Mapping Rek Pajak',       'link' => 'parameter/mapping_rekening_pajak',     'icon' => 'credit-card',  'path' => 'Mapping Rekening Pajak' ];
//     $rekening                                   = [ 'name' => 'Rekening',                'link' => 'parameter/rekening',                   'icon' => 'credit-card',  'path' => 'Rekening' ];
//     $peraturan_terkait                          = [ 'name' => 'Peraturan Terkait',       'link' => 'parameter/peraturan_terkait',          'icon' => 'info',         'path' => 'Peraturan Terkait' ];
//     $penandatangan_tangan_dokumen               = [ 'name' => 'Tandatangan Dokumen',     'link' => 'parameter/penandatanganan_dokumen',    'icon' => 'pencil',         'path' => 'Penandatangan Tangan Dokumen' ];
//     // menu utama
//     $parameter                                  = [ 'name' => 'Parameter',  'link' => [$data_umum_pemda, $unit_organisasi, $data_umum_organisasi, $penandatangan_tangan_dokumen, $peraturan_terkait, $rekening, $mapping_rekening_pajak, $mapping_rekening_retribusi, $kecamatan_kelurahan, $bank, $perhitungan_penyisihan, $perhitungan_pajak, $perhitungan_sanksi, $bphtb],  'icon' => 'pin',  'path' => 'Parameter' ];
//
//
//     // menu help
//     $mansis                                     = [ 'name' => 'ManSis',         'link' => 'help/mansis',     'icon' => 'envelope-letter',       'path' => 'ManSis' ];
//     $contact_us                                 = [ 'name' => 'Contact Us',     'link' => 'help/contact',     'icon' => 'call-in',               'path' => 'Contact Us' ];
//     // menu utama
//     $help                                       = [ 'name' => 'Help',  'link' => [$mansis, $contact_us],  'icon' => 'question',  'path' => 'Help' ];
//
//
//     //menu dashboard
//     $dashboard                                  = [ 'name' => 'Dashboard',     'link' => 'dashboard',        'icon' => 'home',     'path' => 'Dashboard' ];
//     // menu user
//     $user                                       = [ 'name' => 'User',          'link' => 'user',     'icon' => 'user',     'path' => 'User'];
//
//
//     // $menu                                       = [$dashboard, $parameter, $data_entry, $user, $help];
//     // $data_entry                                 = [ 'name' => 'Data Entry',     'link' => [$de_pendaftaran, $de_pendataan, $de_penetapan, $de_penagihan, $de_bendahara_penerimaan, $de_pembukuan_dan_pelaporan],     'icon' => 'note',     'path' => 'Data Entry' ];
//     $menu                                       = [$dashboard, $parameter, $de_pendaftaran, $de_pendataan, $de_bendahara_penerimaan, $de_penetapan, $de_penagihan, $de_pembukuan_dan_pelaporan, $user, $help];
//     // echo "<pre>",print_r($menu),exit();
//
// 	return $menu;
// }
