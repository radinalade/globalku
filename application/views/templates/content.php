<?=@$_css?>
<?=@$_js?>



<!-- BEGIN PAGE TITLE-->
<h3 class="page-title"><?=$pagetitle?>
    <small><?=@$subtitle?></small>
</h3>
<!-- END PAGE TITLE-->
<div class="page-bar note note-info">
    
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a class="ajaxify" href="<?=base_url()?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
      
        </ul>
</div>
<!-- END PAGE HEADER-->

<!-- CONTENT -->

<!-- END CONTENT -->

<script type="text/javascript">
    $(document).ready(function() {
        // reload init
        App.init();
        document.title = '<?php echo $pagetitle ?> | <?php echo $this->config->item( 'app_name' ) ?>';
    });
</script>
