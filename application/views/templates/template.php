<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Metronic | Blank Page Layout</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <?php
        foreach ( $this->config->item('plugin') as $key => $value) {
            echo get_additional( $value, 'css' );
        }
    ?>
 
    <link href="<?=base_url()?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link href="<?=base_url()?>/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?=base_url()?>/assets/global/css/components-rounded.css" rel="stylesheet" type="text/css" id="style_components"/>
    <link href="<?=base_url()?>/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?=base_url()?>/assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>/assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <!-- <link href="<?=base_url()?>/assets/layouts/layout2/css/themes/grey.min.css" rel="stylesheet" type="text/css" id="style_color" /> -->
    <link href="<?=base_url()?>/assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <!-- <link rel="shortcut icon" href="favicon.ico" /> -->

</head>
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?=base_url()?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/fancytree/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript"> var base_url = "<?=base_url()?>"; </script>
    <script src="<?=base_url()?>/assets/global/scripts/app.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

    <!-- HEADER -->
    <?php echo @$_header ?>

    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->

    <!-- BEGIN CONTAINER -->
    <div class="page-container">

        <!-- SIDEBAR -->
        <?php echo @$_sidebar ?>

        <!-- HEADER CONTENT -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

                <!-- FULL CONTENT -->
                <?=$_content?>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

    <!-- FOOTER -->
    <?php echo $_footer ?>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?=base_url()?>/assets/global/plugins/jquery.form.min.js" type="text/javascript"></script>
    <?php echo $_js ?>
    <script src="<?=base_url() ?>/assets/global/scripts/process.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <?php
        foreach ( $this->config->item('plugin') as $key => $value) {
            echo get_additional( $value, 'js' );
        }
    ?>
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?=base_url()?>/assets/layouts/layout2/scripts/layout.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/layouts/layout2/scripts/demo.js" type="text/javascript"></script>
    <script src="<?=base_url()?>/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="<?=base_url() ?>/assets/pages/scripts/custom.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>
