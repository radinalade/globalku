<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo $pagetitle .' - '. $this->config->item( 'app_name' ) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo base_url('assets/layouts/layout/css/open-sans.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet');?>" type="text/css" />
    <link href="<?=base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?=base_url('assets/global/plugins/datatables/datatables.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');?>" rel="stylesheet" type="text/css" />
    <?php
    foreach ( $this->config->item('plugin') as $key => $value) {
        get_additional( $value, 'css' );
    }
    ?>
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?=base_url('assets/global/css/components.min.css');?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?=base_url('assets/global/css/plugins.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?=base_url('assets/layouts/layout/css/layout.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('assets/layouts/layout/css/themes/darkblue.min.css');?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?=base_url('assets/layouts/layout/css/custom.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- ICON -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/global/img') ?>/favicon.png" />
</head>

<!-- BEGIN JQUERY -->
<script src="<?=base_url('assets/global/plugins/jquery.min.js')?>" type="text/javascript"></script>
<!-- BEGIN BASE URL -->
<script src="<?=base_url('assets/global/plugins/fancytree/js/jquery-ui.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
	var base_url = "<?=base_url()?>";
</script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url( 'assets/global/scripts/app.js' ) ?>" type="text/javascript"></script>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" style="background-color: #111;">

    <?php echo @$_header ?>

    <!-- BEGIN CONTAINER -->
    <div class="page-container">

        <?php echo @$_sidebar ?>

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

                <?=$_fullcontent?>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->


    <?php echo $_footer ?>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url( 'assets/global/scripts/process.js ') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/bootstrap/js/bootstrap.min.js ') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/js.cookie.min.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/jquery.form.min.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/jquery.blockui.min.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/global/plugins/select2/js/select2.full.min.js' ) ?>" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <?php
    foreach ( $this->config->item('plugin') as $key => $value) {
        echo get_additional( $value, 'js' );
    }
    ?>
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?php echo base_url( 'assets/layouts/layout/scripts/layout.js' ) ?>" type="text/javascript"></script>
    <script src="<?php echo base_url( 'assets/pages/scripts/custom.js' ) ?>" type="text/javascript"></script>
</body>
</html>
